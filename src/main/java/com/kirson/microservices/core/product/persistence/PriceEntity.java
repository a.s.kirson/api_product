package com.kirson.microservices.core.product.persistence;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "prices")
public class PriceEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "create_time")
    private Instant createTime;

    @Column(name = "price")
    private double price;

    @Column(name = "product_id")
    private long productId;

    public PriceEntity() {

    }

    public PriceEntity(long createTime, double price, long productId)
    {
        this.createTime = Instant.ofEpochMilli(createTime);
        this.price      = price;
        this.productId = productId;
    }

    public Instant getCreateTime()
    {
        return createTime;
    }

    public double getPrice()
    {
        return price;
    }
}
