package com.kirson.microservices.core.product.persistence;

import javax.persistence.*;

@Entity
@Table(name = "stores")
public class StoreEntity
{

  @Id
  @GeneratedValue
  @Column(name = "id")
  private long id;
  @Column(name = "name")
  private String name;

  public StoreEntity() {
  }

  public StoreEntity(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public long getId(){
    return this.id;
  }

  @Override
  public String toString()
  {
    return "StoreEntity{" +
        "id=" + id +
        ", name='" + name + '\'' +
        '}';
  }
}
