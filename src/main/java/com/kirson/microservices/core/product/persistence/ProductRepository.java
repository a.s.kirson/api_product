package com.kirson.microservices.core.product.persistence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<ProductEntity, Long> {
    List<ProductEntity> findAll();

    ProductEntity getProductEntityByNameAndStoreId(String name, long storeId);
}
