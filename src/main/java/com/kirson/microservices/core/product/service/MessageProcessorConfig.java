package com.kirson.microservices.core.product.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kirson.microservices.core.product.model.Product;
import com.kirson.microservices.core.product.persistence.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.function.Consumer;

@Configuration
public class MessageProcessorConfig
{
  private static final Logger Log = LoggerFactory.getLogger(MessageProcessorConfig.class);

  private final StoreRepository storeRepository;
  private final ProductRepository productRepository;
  private final PriceRepository priceRepository;

  @Autowired
  public MessageProcessorConfig(
      StoreRepository storeRepository,
      ProductRepository productRepository,
      PriceRepository priceRepository
  )
  {
    this.storeRepository = storeRepository;
    this.productRepository = productRepository;
    this.priceRepository = priceRepository;
  }

  @Bean
  public Consumer<String> messageProcessor()
  {
    return event -> {
      try {
        Product product = new ObjectMapper().readValue(event, Product.class);

        Log.info("Product {}", product);

        StoreEntity storeEntity = storeRepository.getByName(product.storeName());

        Log.info("StoreEntity {}", storeEntity);

        ProductEntity productEntity = productRepository
                .getProductEntityByNameAndStoreId(product.name(), storeEntity.getId());

        if (productEntity != null) {
          PriceEntity priceEntity = new PriceEntity(product.createTime(), product.price(), productEntity.getId());
          Log.info("priceEntity {}", priceEntity);
          priceRepository.save(priceEntity);
        }
      } catch (JsonProcessingException e) {
        e.printStackTrace();
      }
    };
  }
}
