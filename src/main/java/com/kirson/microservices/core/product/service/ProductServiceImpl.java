package com.kirson.microservices.core.product.service;

import com.kirson.microservices.core.product.api.ProductService;
import com.kirson.microservices.core.product.persistence.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProductServiceImpl implements ProductService
{
  private final PriceRepository priceRepository;
  private final ProductRepository productRepository;
  private final StoreRepository storeRepository;

  @Autowired
  public ProductServiceImpl(
          PriceRepository priceRepository,
          ProductRepository productRepository,
          StoreRepository storeRepository
  )
  {
    this.priceRepository = priceRepository;
    this.productRepository = productRepository;
    this.storeRepository = storeRepository;
  }

  @Override
  public List<PriceEntity> getPricesByProductId(long productId)
  {
    return priceRepository.getAllByProductId(productId);
  }

  @Override
  public List<ProductEntity> getAllProduct()
  {
    return productRepository.findAll();
  }

  @Override
  public List<StoreEntity> getAllStore() {
    return storeRepository.findAll();
  }
}
