package com.kirson.microservices.core.product.persistence;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PriceRepository extends CrudRepository<PriceEntity, Long> {

//    @Query(value = "SELECT price, create_time FROM prices WHERE sku_id = (SELECT (id) FROM skus WHERE name=:skuName)",
//            nativeQuery = true)
    List<PriceEntity> getAllByProductId(long productId);
}
