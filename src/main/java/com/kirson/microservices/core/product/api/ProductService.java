package com.kirson.microservices.core.product.api;

import com.kirson.microservices.core.product.persistence.PriceEntity;
import com.kirson.microservices.core.product.persistence.ProductEntity;
import com.kirson.microservices.core.product.persistence.StoreEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface ProductService {

    @GetMapping(
        value = "/price/{productId}",
        produces = "application/json")
    List<PriceEntity> getPricesByProductId(@PathVariable long productId);

    @GetMapping(
        value = "/product",
        produces = "application/json"
    )
    List<ProductEntity> getAllProduct();

    @GetMapping(
            value = "/store",
            produces = "application/json"
    )
    List<StoreEntity> getAllStore();
}
