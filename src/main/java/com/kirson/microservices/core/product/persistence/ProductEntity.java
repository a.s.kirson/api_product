package com.kirson.microservices.core.product.persistence;

import javax.persistence.*;

@Entity
@Table(name = "products")
public class ProductEntity {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private long id;
    @Column(name = "name")
    private String name;
    @Column(name = "store_id")
    private long storeId;

    public ProductEntity() {
    }

    public ProductEntity(String name, long storeId) {
        this.name = name;
        this.storeId = storeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getStoreId() {
        return storeId;
    }

    public long getId(){
        return this.id;
    }

    @Override
    public String toString()
    {
        return "ProductEntity{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", storeId=" + storeId +
            '}';
    }
}
