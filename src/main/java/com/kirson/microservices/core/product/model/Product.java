package com.kirson.microservices.core.product.model;

public record Product(String name, String storeName, double price, long createTime)
{
}
