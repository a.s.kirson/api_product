package com.kirson.microservices.core.product.persistence;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StoreRepository extends CrudRepository<StoreEntity, Long> {

    StoreEntity getByName(String name);

    @Query(value = "SELECT * FROM stores ", nativeQuery = true)
    List<StoreEntity> getAll();

    List<StoreEntity> findAll();
}
