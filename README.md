# Getting Started

### Run project
- In project dir use command ./gradlew build && docker-compose build && docker-compose up -d
- First run crawler project, second run api_product project

### API End points 

- Get all stores 
- GET http://localhost:8080/store
- Response
  [
  {
  "id": 1,
  "name": "Giperglobus"
  }
  ]

- Get all prices by product id
- GET http://localhost:8080/price/1
- Response
  [
  {
  "createTime": "2019-01-01T00:00:00Z",
  "price": 90.56
   }, 
   {
  "createTime": "2019-01-02T00:00:00Z",
  "price": 88.56
   }
  ]

- Get all products
- GET http://localhost:8080/product

[
{
"id":1,
"name":"Картофель, 1 кг",
"storeId":1
},
{
"id":2,
"name":"Томаты красные на ветке, 1 кг",
"storeId":1
}
]