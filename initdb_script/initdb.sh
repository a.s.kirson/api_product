#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "docker_user" --dbname "inflation" <<-EOSQL
    GRANT ALL PRIVILEGES ON DATABASE inflation TO docker_user;

CREATE TABLE STORES(
 ID SERIAL PRIMARY KEY,
 NAME VARCHAR(100) UNIQUE
);

CREATE TABLE PRODUCTS (
 ID SERIAL PRIMARY KEY,
 NAME VARCHAR(250) UNIQUE,
 STORE_ID INTEGER,
 FOREIGN KEY(STORE_ID) REFERENCES STORES(ID)
);

CREATE TABLE PRICES (
 ID SERIAL PRIMARY KEY,
 CREATE_TIME TIMESTAMP,
 PRICE DOUBLE PRECISION,
 PRODUCT_ID INTEGER NOT NULL,
 FOREIGN KEY(PRODUCT_ID) REFERENCES PRODUCTS (ID)
);

INSERT INTO STORES (ID, NAME)

VALUES (1, 'Giperglobus');

INSERT INTO PRODUCTS (ID, NAME, STORE_ID)

SELECT ID,
    (ARRAY ['Картофель, 1 кг',
    'Томаты красные на ветке, 1 кг',
    'Томаты розовые, 1 кг',
    'Огурцы среднеплодные, 1 кг',
    'Огурец гладкий, 1 кг',
    'Капуста белокочанная, 1 кг',
    'Свёкла, 1 кг',
    'Морковь, 1 кг',
    'Лук репчатый, 1 кг',
    'Чеснок, 1 кг',
    'Сахар-песок, 1 кг',
    'Соль поваренная пищевая каменная, 1 кг',
    'Масло сливочное Брест-Литовск 82,5%, 180 г',
    'Мука пшеничная Makfa высший сорт, 2 кг',
    'Макаронные изделия Витой рожок Шебекинские, 450 г',
    'Крупа гречневая Мистраль ядрица, 900 г',
    'Говяжья нога на кости, 1 кг',
    'Свиной окорок, 1 кг',
    'Бедро цыплёнка-бройлера охлаждённое Моссельпром с кожей, 1 кг',
    'Молоко пастеризованное Простоквашино отборное 3,4-4,5%, 930 мл',
    'Молоко ультрапастеризованное Parmalat 3,5%, 1 л',
    'Масло подсолнечное Золотая Семечка рафинированное, 1 л'])[ID],
     1

FROM GENERATE_SERIES(1, 22) ID;

-- Generate synthetic data with price range as >= 75 and < 100 for every sku fro three years

INSERT INTO PRICES(CREATE_TIME, PRICE, PRODUCT_ID)

SELECT GENERATE_SERIES(date '2019-01-01', date '2021-01-31', '1 day'),
      ROUND((((RANDOM()*(100-75+1))+75)::NUMERIC), 2),
       PRODUCT_ID

FROM GENERATE_SERIES(1, 22) PRODUCT_ID

EOSQL